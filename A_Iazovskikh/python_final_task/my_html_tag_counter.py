import requests
from lxml import html
from collections import defaultdict
from datetime import datetime
import boto3

def count_tags(url: str):
    """
    This function which count numbers of tags on the HTML page
    :param url: a string with link to a HTML webpage
    :return: dictionary where Key='HTML Tag' and Value='Number of tag'
    """
    page = requests.get(url)
    tree = html.fromstring(page.content)
    all_elms = tree.cssselect('*')
    dict_of_tags = defaultdict(int)
    for key in all_elms:
        dict_of_tags[key.tag] += 1
    return dict(dict_of_tags)

def logging(dict_of_tags: dict, url: str, log_file_path: str):
    """
    This function compose an F string with logging information and write it to local log file.
    :param dict_of_tags: a dictionary with Key='HTML Tag' and Value='Tag's number on the page'
    :param url: a string with link to a HTML webpage
    :param log_file_path: path to logging file
    :return: an F string with logging information
    """
    all_tag_num = sum(dict_of_tags.values())
    log_time = datetime.now().strftime("%Y/%m/%d %H:%M")
    log_str = f'{log_time}  {url}  {all_tag_num} {dict(dict_of_tags)}\n'
    with open(log_file_path, 'a') as f:
        f.write(log_str)
    return log_str

def copy_log_to_s3(bucket_name: str, log_file_path: str):
    """
    This function create bucket in S3 if it doesn't exist and upload the local log file to it
    :param bucket_name: a string with bucket name
    :param log_file_path: a string with path to log file
    :return:
    """
    s3 = boto3.resource('s3')
    s3.create_bucket(Bucket=bucket_name)
    s3.meta.client.upload_file(log_file_path, bucket_name, log_file_path.split("/")[-1])

def main_tag_counter(url: str, bucket_name: str, log_file_path: str):
    """
    This is the main function which call other functions to perform all required actions in final task of Python Course
    :param url: a string with link to a HTML webpage
    :param bucket_name: a string with bucket name
    :param log_file_path: a string with path to log file
    :return:
    """
    dict_of_tags = count_tags(url)
    log_str = logging(dict_of_tags, url, log_file_path)
    print(log_str)
    copy_log_to_s3(bucket_name, log_file_path)

# Initialize variables
url = input('Enter a hyperlink to a webpage for counting tags: ')
bucket_name = input('Enter target s3 Bucket name: ')
log_file_path = './log.txt'

# Call main function
main_tag_counter(url, bucket_name, log_file_path)

