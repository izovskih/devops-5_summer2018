name "database"
description "This role specifies all recipes to deploy RDS database using CloudFormation"

run_list(
  "recipe[final-ha-deploy::db]"
)