name "webserver"
description "This role specifies all recipes to deploy Wordpress using CloudFormation and Chef Provisioning of EC2 Instance"

run_list(
  "recipe[final-ha-deploy::wp]"
)