name "loadbalancer"
description "This role specifies all recipes to deploy ELB using CloudFormation"

run_list(
  "recipe[final-ha-deploy::lb]"
)