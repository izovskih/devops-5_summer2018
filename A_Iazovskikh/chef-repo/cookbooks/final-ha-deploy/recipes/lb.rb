#
# Cookbook:: final-ha-deploy
# Recipe:: db
#
# Copyright:: 2018, The Authors, All Rights Reserved.

aws_cloudformation_stack 'izovskih-wp-stack' do
  action :create
  template_source 'izovskih-wp-stack_lb.tpl'
  region 'us-east-1'
end
