#
# Cookbook:: final-ha-deploy
# Recipe:: wp
#
# Copyright:: 2018, The Authors, All Rights Reserved.

aws_cloudformation_stack 'izovskih-wp-stack-wp' do
  action :create
  template_source 'izovskih-wp-stack_wp.tpl'
  region 'us-east-1'
  disable_rollback true
  parameters ([
    {
      :parameter_key => 'KeyName',
      :parameter_value => 'kp-izovskih-test-1'
    },
    {
      :parameter_key => 'DBUser',
      :parameter_value => 'WPDBUser'
    },
    {
      :parameter_key => 'DBPassword',
      :parameter_value => 'DBPassword'
    },
    {
      :parameter_key => 'WebServerCapacity',
      :parameter_value => '2'
    }
  ])
end
