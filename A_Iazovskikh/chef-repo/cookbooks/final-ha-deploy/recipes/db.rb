#
# Cookbook:: final-ha-deploy
# Recipe:: db
#
# Copyright:: 2018, The Authors, All Rights Reserved.

aws_cloudformation_stack 'izovskih-wp-stack-db' do
  action :create
  template_source 'izovskih-wp-stack_db.tpl'
  region 'us-east-1'
  parameters ([
    {
      :parameter_key => 'DBUser',
      :parameter_value => 'WPDBUser'
    },
    {
      :parameter_key => 'DBPassword',
      :parameter_value => 'DBPassword'
    }
  ])
end
