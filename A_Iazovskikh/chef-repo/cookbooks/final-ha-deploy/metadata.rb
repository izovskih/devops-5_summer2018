name 'final-ha-deploy'
maintainer 'Aleksey Yazovskikh'
maintainer_email 'izovskih@gmail.com'
license 'Apache-2.0'
description 'Installs/Configures final-ha-deploy'
long_description 'Installs/Configures final-ha-deploy'
version '0.1.0'
chef_version '>= 12.14' if respond_to?(:chef_version)

supports 'debian'

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
# issues_url 'https://github.com/<insert_org_here>/final-ha-deploy/issues'
issues_url 'https://bitbucket.org/izovskih/devops-5_summer2018/src/master/A_Iazovskikh/chef-repo/'

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
# source_url 'https://github.com/<insert_org_here>/final-ha-deploy'
source_url 'https://bitbucket.org/izovskih/devops-5_summer2018/src/master/A_Iazovskikh/chef-repo/'

depends 'aws', '~> 7.5.0'