{
  "AWSTemplateFormatVersion" : "2010-09-09",

  "Description" : "AWS Cloudformation create ElasticLoadBalancer",

  "Parameters" : {

    "SSHLocation": {
      "Description": "The IP address range that can be used to SSH to the EC2 instances",
      "Type": "String",
      "MinLength": "9",
      "MaxLength": "18",
      "Default": "0.0.0.0/0",
      "AllowedPattern": "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})",
      "ConstraintDescription": "must be a valid IP CIDR range of the for x.x.x.x/x."
    }
  },

  "Resources" : {

    "ElasticLoadBalancer" : {
      "Type" : "AWS::ElasticLoadBalancing::LoadBalancer",
      "Metadata" : {
        "Comment1" : "Configure the Load Balancer with a simple health check and cookie-based stickiness",
        "Comment2" : "Use install path for healthcheck to avoid redirects - ELB healthcheck does not handle 302 return codes"
      },
      "Properties" : {
        "AvailabilityZones" : { "Fn::GetAZs" : "" },
        "CrossZone" : "true",
        "LBCookieStickinessPolicy" : [ {
          "PolicyName" : "CookieBasedPolicy",
          "CookieExpirationPeriod" : "30"
        } ],
        "Listeners" : [ {
          "LoadBalancerPort" : "80",
          "InstancePort" : "80",
          "Protocol" : "HTTP",
          "PolicyNames" : [ "CookieBasedPolicy" ]
        } ],
        "HealthCheck" : {
          "Target" : "HTTP:80/wp-admin/install.php",
          "HealthyThreshold" : "2",
          "UnhealthyThreshold" : "5",
          "Interval" : "10",
          "Timeout" : "5"
        }
      }
    },

    "WebServerSecurityGroup" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "GroupDescription" : "Enable HTTP access via port 80 locked down to the load balancer + SSH access",
        "SecurityGroupIngress" : [
          { "IpProtocol" : "tcp",
            "FromPort" : "80",
            "ToPort" : "80",
            "SourceSecurityGroupOwnerId" : {
              "Fn::GetAtt" : [ "ElasticLoadBalancer",
                               "SourceSecurityGroup.OwnerAlias"
              ]
            },
            "SourceSecurityGroupName" : {
              "Fn::GetAtt" : [ "ElasticLoadBalancer",
                               "SourceSecurityGroup.GroupName"
              ]
            }
          },
          { "IpProtocol" : "tcp",
            "FromPort" : "22",
            "ToPort" : "22",
            "CidrIp" : { "Ref" : "SSHLocation"}
          }
        ]
      }
    },

  },
  "Outputs" : {
    "WebsiteURL" : {
      "Value" : { "Fn::Join" : ["", ["http://", { "Fn::GetAtt" : [ "ElasticLoadBalancer", "DNSName" ]}]]},
      "Description" : "WordPress website"
    },
    "ElasticLoadBalancer" : {
      "Description" : "The ELB ID to use for public web servers",
      "Value" :  { "Ref" : "ElasticLoadBalancer" },
      "Export" : { "Name" : "izovskih-wp-stack-ELB"}
    },
    "WebServerSecurityGroup" : {
      "Description" : "The web server security group",
      "Value" :  { "Ref" : "WebServerSecurityGroup" },
      "Export" : { "Name" : "izovskih-wp-stack-WSSecGroup"}
    },
  }
}