{
  "AWSTemplateFormatVersion" : "2010-09-09",

  "Description" : " Wordpress deploy",

  "Parameters" : {

    "KeyName": {
      "Description" : "Name of an existing EC2 key pair to enable SSH access to the instances",
      "Type": "AWS::EC2::KeyPair::KeyName",
      "ConstraintDescription" : "must be the name of an existing EC2 KeyPair."
    },

    "InstanceType" : {
      "Description" : "Web Server EC2 instance type",
      "Type" : "String",
      "Default" : "t2.small",
      "AllowedValues" : [ "t1.micro", "t2.nano", "t2.micro", "t2.small", "t2.medium", "t2.large", "m1.small", "m1.medium", "m1.large", "m1.xlarge", "m2.xlarge", "m2.2xlarge", "m2.4xlarge", "m3.medium", "m3.large", "m3.xlarge", "m3.2xlarge", "m4.large", "m4.xlarge", "m4.2xlarge", "m4.4xlarge", "m4.10xlarge", "c1.medium", "c1.xlarge", "c3.large", "c3.xlarge", "c3.2xlarge", "c3.4xlarge", "c3.8xlarge", "c4.large", "c4.xlarge", "c4.2xlarge", "c4.4xlarge", "c4.8xlarge", "g2.2xlarge", "g2.8xlarge", "r3.large", "r3.xlarge", "r3.2xlarge", "r3.4xlarge", "r3.8xlarge", "i2.xlarge", "i2.2xlarge", "i2.4xlarge", "i2.8xlarge", "d2.xlarge", "d2.2xlarge", "d2.4xlarge", "d2.8xlarge", "hi1.4xlarge", "hs1.8xlarge", "cr1.8xlarge", "cc2.8xlarge", "cg1.4xlarge"],
      "ConstraintDescription" : "must be a valid EC2 instance type."
    },

    "DBName" : {
      "Default": "wordpressdb",
      "Description" : "The WordPress database nae",
      "Type": "String",
      "MinLength": "1",
      "MaxLength": "64",
      "AllowedPattern" : "[a-zA-Z][a-zA-Z0-9]*",
      "ConstraintDescription" : "must begin with a letter and contain only alphanumeric characters."
    },

    "DBUser" : {
      "NoEcho": "true",
      "Description" : "The WordPress database admin account user name",
      "Type": "String",
      "MinLength": "1",
      "MaxLength": "16",
      "AllowedPattern" : "[a-zA-Z][a-zA-Z0-9]*",
      "ConstraintDescription" : "must begin with a letter and contain only alphanumeric characters."
    },

    "DBPassword" : {
      "NoEcho": "true",
      "Description" : "The WordPress database admin account password",
      "Type": "String",
      "MinLength": "8",
      "MaxLength": "41",
      "AllowedPattern" : "[a-zA-Z0-9]*",
      "ConstraintDescription" : "must contain only alphanumeric characters."
    },

    "WebServerCapacity": {
      "Default": "1",
      "Description" : "The initial number of web server instances",
      "Type": "Number",
      "MinValue": "1",
      "MaxValue": "5",
      "ConstraintDescription" : "must be between 1 and 5 EC2 instances."
    }

  },

  "Mappings": {

    "RegionMap": {
      "us-east-1": {
        "AMI": "ami-0ff8a91507f77f867"
      },
      "us-east-2": {
        "AMI": "ami-0b59bfac6be064b78"
      },
      "us-west-1": {
        "AMI": "ami-0bdb828fd58c52235"
      },
      "us-west-2": {
        "AMI": "ami-a0cfeed8"
      }
    }
  },

  "Resources" : {

    "WebServerGroup" : {
      "Type" : "AWS::AutoScaling::AutoScalingGroup",
      "Properties" : {
        "AvailabilityZones" : { "Fn::GetAZs" : "" },
        "LaunchConfigurationName" : { "Ref" : "LaunchConfig" },
        "MinSize" : "1",
        "MaxSize" : "5",
        "DesiredCapacity" : { "Ref" : "WebServerCapacity" },
        "LoadBalancerNames" : [ {"Fn::ImportValue": "izovskih-wp-stack-ELB"} ]
      },
      "CreationPolicy" : {
        "ResourceSignal" : {
          "Timeout" : "PT10M"
        }
      },
      "UpdatePolicy": {
        "AutoScalingRollingUpdate": {
          "MinInstancesInService": "2",
          "MaxBatchSize": "2",
          "PauseTime" : "PT10M",
          "WaitOnResourceSignals": "true"
        }
      }
    },

    "LaunchConfig": {
      "Type" : "AWS::AutoScaling::LaunchConfiguration",
      "Metadata" : {
        "AWS::CloudFormation::Init" : {
          "configSets" : { "order"	: ["chefversion"] },

          "chefversion" : {
            "packages" : {
              "rubygems" : { "chef" : ["0.10.2"] },
              "yum" : {
                "gcc-c++"    : [],
                "ruby-devel" : [],
                "make"       : [],
                "autoconf"   : [],
                "automake"   : [],
                "rubygems"   : []
              }
            },

            "files" : {

              "/etc/chef/solo.rb" : {
                "content" : { "Fn::Join" : ["\n", [
                  "log_level :info",
                  "log_location STDOUT",
                  "file_cache_path \"/var/chef-solo\"",
                  "cookbook_path \"/var/chef-solo/cookbooks\"",
                  "json_attribs \"/etc/chef/node.json\"",
                  "recipe_url \"https://s3.amazonaws.com/cloudformation-examples/wordpress.tar.gz\""
                ]]},
                "mode" : "000644",
                "owner" : "root",
                "group" : "root"
              },

              "/etc/chef/node.json" : {
                "content" : {
                  "wordpress" : { "db" : {
               	    "database" : {"Ref" : "DBName"},
               	    "user"     : {"Ref" : "DBUser"},
                   	"host"     : {"Fn::ImportValue": "izovskih-wp-stack-DBAddr"},
                   	"password" : {"Ref" : "DBPassword" }
                  }},
                  "run_list": [ "recipe[wordpress]" ]
                },
                "mode" : "000644",
                "owner" : "root",
                "group" : "root"
              }
            }
          }
        }
      },

      "Properties": {

        "ImageId":      { "Fn::FindInMap": ["RegionMap", { "Ref": "AWS::Region" }, "AMI"] },
        "InstanceType": { "Ref": "InstanceType" },
        "SecurityGroups" : [ {"Fn::ImportValue": "izovskih-wp-stack-WSSecGroup"} ],
        "KeyName":      { "Ref": "KeyName" },

        "UserData" : { "Fn::Base64" : { "Fn::Join" : ["", [
          "#!/bin/bash\n",
          "sudo yum update -y aws-cfn-bootstrap\n",
          "sudo /opt/aws/bin/cfn-init -v",
          "         --stack ", { "Ref" : "AWS::StackName" },
          "         --resource LaunchConfig ",
          "         --configsets order ",
          "         --region ", { "Ref" : "AWS::Region" },
          " && ",
          "sudo chef-solo\n",
          "sudo /opt/aws/bin/cfn-signal -e $? ",
          "         --stack ", { "Ref" : "AWS::StackName" },
          "         --resource WebServerGroup ",
          "         --region ", { "Ref" : "AWS::Region" },
          "\n"
        ]]}}

      }
    }
  }
}