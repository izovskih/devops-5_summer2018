# final-ha-deploy

Description
===========
chef cookbook for implement wordpress on AWS HA infrastructure


Changes
=======


## v 0.0.0

Requirements
============

## Platform:

* debian
* centos

## Cookbooks:

* aws

Attributes
==========

Recipes
=======


default
-------


Resources/Providers
===================


Usage
=====


Examples
--------

License and Author
==================

Maintainer:: Aleksey Yazovskikh

Copyright:: 2018, izovskih

License:: Apache 2.0
********
Licensed under the Apache License, Version 2.0 (the 'License'); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an 'AS IS' BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
