# Usage: run this script by root user
# This script creates users without password and a directory structure in the root(/), where:

# |-----------|-------------|-----------|-------------|
# | Directory | Read/Write  | Read only | Inheritance |
# |-----------|-------------|-----------|-------------|
# | Proj1     | R2 R3 R5 A1 | A4        | Yes         |
# | Proj2     | R1 R5 A1    | A2 A3     | Yes         |
# | Proj3     | R1 R2 R4 A2 | A1 A4     | Yes         |
# |-----------|-------------|-----------|-------------|
# | DIRS[]    | USR_RW[]    | USR_R[]   |             |
# |-----------|-------------|-----------|-------------|

# I1, I2 and I3 users should be able to create a file in any directory
# but they should not be allowed to delete anything.

# Specify the directory names separated by spaces
DIRS=(Proj1 Proj2 Proj3)

# Users with access to Read/Write to the corresponding directory
declare -A USR_RW=( [${DIRS[0]}]="R2 R3 R5 A1" 
		    [${DIRS[1]}]="R1 R5 A1" 
		    [${DIRS[2]}]="R1 R2 R4 A2")

# Users with access to Read only to the corresponding directory
declare -A USR_R=(  [${DIRS[0]}]="A4" 
		    [${DIRS[1]}]="A2 A3"
		    [${DIRS[2]}]="A1 A4")

for R in R{1..5}; do
 useradd $R
 passwd -d $R
done

for A in A{1..4}; do
 useradd $A
 passwd -d $A
done

for I in I{1..3}; do
 useradd $I
 passwd -d $I
done

for DIR in ${DIRS[@]}; do
 mkdir /$DIR
 groupadd ${DIR,,}
 chown :${DIR,,} /$DIR
 chmod 3770 /$DIR
 for U in ${USR_RW[$DIR]}; do
  gpasswd -a $U ${DIR,,}
 done
 setfacl -d -m u::7,g::7,o::0 /$DIR
 for U in ${USR_R[$DIR]}; do
  setfacl -d -m u:$U:5 /$DIR
  setfacl -m u:$U:5 /$DIR
 done
 for U in I{1..3}; do
  setfacl -d -m u:$U:rwx /$DIR
  setfacl -m u:$U:rwx /$DIR
 done
done
