{
  arrHits[int($0/10)]++
}
END {
  strRange="  0 -  9:; 10 - 19:; 20 - 29:; 30 - 39:; 40 - 49:; 50 - 59:; 60 - 69:; 70 - 79:; 80 - 89:; 90 - 99:;100:"
  split(strRange, arrRange, ";")
  printf("\n%9s%-s\n", "RANGE:", "  Percentage of HITs to RANGE (%)")
  for (i=0;i<=10;i++){
   avg=(arrHits[i]*100)/NR
   strStar=gensub(/ /, "*", "g", sprintf("%*s", avg, ""))
   printf("%-9s%4.f %-s\n", arrRange[i+1], avg, strStar)
  }
}
