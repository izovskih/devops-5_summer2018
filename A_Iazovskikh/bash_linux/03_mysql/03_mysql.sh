#!/bin/bash

#   The Script has been successfully run on CentOS Linux release 7.5.1804 (Core)
###   This script need root's rights, you should be root to run this script or use sudo
#     Usage: sudo ./03_mysql.sh
### This script to do:
#   1. Create ./log.log
#   2. Install all needs repositories (internet connection need)
#   3. Install components: (internet connection need)
#   3.1 Expect
#   3.2 Percona MYSQL server
#   3.3 PHP
#   3.4 Apache Web server
#   3.5 phpmyadmin
#   4. Enable autostart and start all installed demons
#   5. Configure MYSQL server
#   5.1 Change temporary password for root (set new root password with $MYSQL_ROOT_PASS)
#   5.2 Remove anonymous users
#   5.3 Disallow root login remotely
#   5.4 Remove test database and access to it
#   5.5 Reload privilege tables now
#   6. Configure /etc/httpd/conf.d/phpMyAdmin.conf to allow connections from LAN
#   7. Restart Apache Web Server
#   8. Add allow inbound 80/tcp rule with firewalld
#   9. Restart firewalld.

function install () {
# Function for install software
# Usage: install software_name
  echo "Installing $1..."
  yum install -y $1 >>$LOG 2>&1 \
        && echo -e "\e[32m--> $1 has been successfully installed\e[0m" \
        || echo -e "\e[31m--> Ooops, can't install $1. See $LOG\e[0m"
}

function confirm () {
# Function for confirming that daemon running
# Usage: confirm daemon_name
  local NAME=$(echo $1 | sed 's/^\(.\)\(.*\)/[\1]\2/') # example --> [e]xample 
  local STAT=$(ps aux | grep $NAME | wc -l)
  if [[ $STAT -eq 0 ]]; then {
      echo "--> Running $1"; sleep 1
      sudo systemctl restart $1 >> $LOG 2>&1 \
          && echo -e "\e[32m--> $1 has been started\e[0m" \
          || echo -e "\e[31m--> Ooops, starting $1 failed. See $LOG\e[0m"
  }
  else {
      echo -e "\e[32m--> $1 has already been run!\e[0m"
  }
  fi
}


# create empty log.log
LOG=$(pwd)/log.log
echo > $LOG && echo "$LOG has been created"

# define links to all needs REPOs
PERCONA_REPO='http://www.percona.com/downloads/percona-release/redhat/0.1-4/percona-release-0.1-4.noarch.rpm'
EPEL_REPO='https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm'

# install REPOs
install $PERCONA_REPO
install $EPEL_REPO

# install server side services
install expect
install Percona-Server-server-57
install phpmyadmin
install httpd
install php
install php-fpm
install php-mysql

# enable autostart server side services
systemctl enable mysqld >> $LOG 2>&1
systemctl enable httpd >> $LOG 2>&1
systemctl enable php-fpm >> $LOG 2>&1

# Confirming that MYSQL is running
confirm mysqld

# define passwords for MYSQL
MYSQL_TEMP_ROOT_PASS=$(sudo grep "temporary password" /var/log/mysqld.log \
                        | awk '{print $NF}' \
                        | tail -1)
MYSQL_ROOT_PASS=QWE123qwe!

# Run the mysql_secure_installation script
expect -c "
    set timeout 1
    spawn mysql_secure_installation
    expect \"Enter password for user root:\"
        send \"$MYSQL_TEMP_ROOT_PASS\n\"
    expect \"New password:\"
        send \"$MYSQL_ROOT_PASS\n\"
    expect \"Re-enter new password:\"
        send \"$MYSQL_ROOT_PASS\n\"
    expect \"Remove anonymous users?\"
        send \"y\n\"
    expect \"Disallow root login remotely?\"
        send \"y\n\"
    expect \"Remove test database and access to it?\"
        send \"y\n\"
    expect \"Reload privilege tables now?\"
        send \"y\n\"
    expect eof
" >> $LOG 2>&1 && echo -e "\e[32m--> MYSQL has been configured\e[0m" \
               || echo -e "\e[31m--> Ooops, can't configure MYSQL. See $LOG\e[0m"

# Configure phpMyAdmin.conf for access from LAN
IP_NETS="10.0.0.0/8 172.16.0.0/12 192.168.0.0/16"
sudo sed -in "s|127.0.0.1|$IP_NETS|g" /etc/httpd/conf.d/phpMyAdmin.conf $LOG 2>&1
sudo systemctl restart httpd >> $LOG 2>&1

# Confirming that HTTPD is running
confirm httpd

# Open incoming HTTP 
firewall-cmd --permanent --add-port=80/tcp >> $LOG 2>&1
firewall-cmd --reload >> $LOG 2>&1

IP_ADDR=$(ip route get 1 | awk '{print $NF;exit}')
echo "-->  If all right you should successfully open http://$IP_ADDR/phpmyadmin"
echo "-->  Use username root and password $MYSQL_ROOT_PASS. You should change root password after login"
