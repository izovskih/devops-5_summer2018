#   This script create Apache configuration from a template 
# with replacement {patterns} to relevant $variables.
# ====================================
# #   Patterns           # Variables #
# ====================================
# # {{IP_ADDR}}          # $IP_ADDR  #
# # {{TCP_PORT}}         # $TCP_PORT #
# # {{USER}}             # $USER     #
# # {{HOSTNAME}}         # $HOSTNAME #
# # {{ROOT_PATH}}        # $HOME/www #
# # {{NOT_USED_PATTERN}} # EMPTY     #
# ====================================

# Скрипт создания конфигурационного файла из шаблона. Соблюдены следующие условия:
#  - Скрипт должен принимать параметры командной строки имея шаблона и имя результирующего файла
#  - Проверять правильность передоваемых в скрипт параметров
#  - В шаблоне должны быть заданны pattern'ы которые с помощью скрипта должны быть заменены значениями соответствующих переменных
#  - В качестве переменных значения которых подставляются в шаблон скрипт должен использовать переменные окружения и другие переменные заданы в bash сессии
#  - Если для какого-то pattern'а не находится ответствующая ему переменная, то вместо его подставляется "пусто"
#  - Учесть возможность того что в качестве значений переменных могут быть имена файлов или директорий
#  - Обеспечить возможность подключать этот скрипт как модуль(библиотеку) в другой bash скрипт

function generate_conf () {
  if [[ $# -ne 2 ]]; then
    echo -e "\e[31m-->  Error, incorrect usage.\e[0m" >&2
    echo "     Usage: ./$(basename $0) template_file output_file" >&2
    exit 1
  fi

  local FILE_TMP=$1
  local FILE_OUT=$2

  if [[ ! -f "$FILE_TMP" ]]; then
    echo -e "\e[31m-->  Error, Couldn't find the template file \"$FILE_TMP\"\e[0m" >&2
    echo >&2
    echo "     Files in the current directory:" >&2
    ls -1 >&2
    echo >&2
    exit 2
  fi

  local TCP_PORT=80
  local IP_ADDR=$(ip route get 1 | awk '{print $NF;exit}')
  local ROOT_PATH=$HOME/www

  sed -e "s|{{IP_ADDR}}|$IP_ADDR|g" \
      -e "s|{{TCP_PORT}}|$TCP_PORT|g" \
      -e "s|{{USER}}|$USER|g" \
      -e "s|{{HOSTNAME}}|$HOSTNAME|g" \
      -e "s|{{ROOT_PATH}}|$ROOT_PATH|g" \
      -e "s|{{.*}}||g" $FILE_TMP > $FILE_OUT

  echo -e "\e[32m-->  You can find output file here: $(pwd)/$FILE_OUT\e[0m"
}

generate_conf $1 $2
