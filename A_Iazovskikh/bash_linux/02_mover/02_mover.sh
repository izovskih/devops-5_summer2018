#!/bin/bash

function create_dir(){
  # this function get one parameter (path to new folder) and create it
  if [ ! -d  "$1" ]; then
    echo "--> Directory $1 isn't exist. Let's create it."
    mkdir "$1" >> $LOG 2>&1 && echo -e "\e[32m--> Directory $1 has been created\e[0m" \
                            || { echo -e "\e[31m--> Can't create $1. See $LOG\e[0m"; exit 1; }
  else
    echo "--> Directory $1 is exist."
  fi
}

function move () {
# this function move files from $SRC_DIR to $DST_DIR

  # create empty transfer.PID.log
  TRNS_LOG=$LOG_DIR/transfer.$$.log
  echo "YYYY-MM-DD HH-MM-SS: Status of moved FILE_NAME OBJ_NAME from SRC_DIR to DST_DIR" > $TRNS_LOG \
                   && echo -e "\e[32m--> $TRNS_LOG has been created\e[0m" \
                   || { echo -e "\e[31m--> Can't create $TRNS_LOG\e[0m"; exit 1; }

  # while in the $SRC_DIR at least one file do
  while [[ ! -z $(ls -A $SRC_DIR) ]]; do
    echo "Let's move any file.."
    FILES=($SRC_DIR/*)
    BUSY_FILES=$(lsof +d $SRC_DIR | grep REG | awk -F"/" '{print $NF}')

    for FILE in ${FILES[@]}; do
      FILE_NAME=$(basename $FILE)
      echo "Trying $FILE_NAME"
      if [[ ! $FILE_NAME == "_busy_"* && ! $BUSY_FILES == $FILE_NAME ]]; then
        OBJ_UUID=$(tail -1 $FILE | sed -nr '/.{8}-.{4}-.{4}-.{4}-.{12}/p')
        if [[ ! -z $OBJ_UUID ]]; then
          # renaming the FILE to mark it as BUSY for different instance of the script
          BUSY_FILE=$SRC_DIR/_busy_$FILE_NAME
          mv $FILE $BUSY_FILE

          # Get some info about OBJECT inside the FILE
          OBJ_NAME=$(head -1 $BUSY_FILE | awk '{print $1}')
          OBJ_DATE=$(head -1 $BUSY_FILE | awk '{print $2}')

          # Get the last modification time of FILE with format full date; same as %Y-%m-%d
          FILE_DATE=$(date -r $BUSY_FILE +%F)

          # Moving incoming FILE to $DST_DIR and log it
          OUT_FILE=$DST_DIR/${OBJ_NAME}_${OBJ_DATE}_${FILE_DATE}
          TXT_DONE="Success moved $FILE_NAME $OBJ_NAME from $SRC_DIR to $DST_DIR"
          TXT_FAIL="Failed move file $FILE_NAME with $OBJ_NAME from $SRC_DIR to $DST_DIR"
          mv $BUSY_FILE $OUT_FILE && { echo "$(date "+%F %T"): $TXT_DONE" | tee -a $TRNS_LOG; break; } \
                                  || { echo "$(date "+%F %T"): $TXT_FAIL" | tee -a $TRNS_LOG; exit 1; }
        else
          echo "$FILE_NAME is incorrect. UUID is missing in the last line of the file. Renaming the file and moving it to $LOG_DIR"
          FAIL_FILE=$LOG_DIR/_uuid_fail_$FILE_NAME
          mv $FILE $FAIL_FILE
        fi
      else
        echo "$FILE_NAME is busy"
      fi
    done
  done
}

MYNAME=$(basename $0)
SETTINGS=$1

if [[ $# -ne 1 ]]; then
  echo -e "\e[31m-->  Error, incorrect usage.\e[0m"
  echo "     Usage: ./$MYNAME settings_file"; exit 1
elif [[ ! -f "$SETTINGS" ]]; then
  echo -e "\e[31m-->  Error, Couldn't find a settings_file \"$SETTINGS\"\e[0m"; exit 1
else
  echo "--> $SETTINGS is exist. Let's import it."
  . $SETTINGS && echo -e "\e[32m--> $SETTINGS has been imported\e[0m" \
              || { echo -e "\e[31m--> Can't import $SETTINGS\e[0m"; exit 1; }
fi

# create dir for logs
echo "--> Workdir of the script is $(pwd)"
LOG_DIR=$(pwd)/logs
mkdir $LOG_DIR

# create empty script_name.PID.log
LOG=$LOG_DIR/$(basename $0).$$.log
echo > $LOG && echo -e "\e[32m--> $LOG has been created\e[0m" \
            || { echo -e "\e[31m--> Can't create $LOG\e[0m"; exit 1; }

# checking SRC and DST dirs was set and create these dirs
for DIR in $SRC_DIR $DST_DIR; do
  if [[ -z "$DIR" ]]; then
    echo -e "\e[31m--> You should specify source and destination dirertories in $SETTINGS\e[0m"; exit 1
  else
    create_dir $DIR
  fi
done

# call main function if incoming directory not empty
if [ ! "$(ls -A $SRC_DIR)" ]; then
  echo "--> $SRC_DIR is empty."
  exit 0
else
  move
fi
