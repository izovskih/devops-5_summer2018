variable "region" {
    type = "string"
    default = "us-east-1"
}

variable "availability_zones" {
    description = "Availability zones. I added only 3 regions but you can add all"
    type        = "map"
    default     = {
        us-east-1      = "us-east-1a,us-east-1b"
        us-west-2      = "us-west-2a,us-west-2b"
        eu-west-1      = "eu-west-1a,eu-west-1b"
    }
}

variable "ami" {
    description = "Amazon Linux AMI 2018.03.0 (HVM), SSD Volume Type. I added only 3 regions but you can add all"
    type        = "map"
    default     = {
        us-east-1 = "ami-cfe4b2b0"
        us-west-2 = "ami-0ad99772"
        eu-west-1 = "ami-e4515e0e"
    }
}

variable "environment" {
    type = "string"
    default = "FinalTask"
}

variable "key_path" {
    description = "Key path to your RSA|DSA key"
    default     = "C:/kp-izovskih-test-1.pub"
}

variable "user_data" {
    description = "The user data to provide when launching the instance"
    default     = "C:/aws_final_task/user_data.sh"
}
