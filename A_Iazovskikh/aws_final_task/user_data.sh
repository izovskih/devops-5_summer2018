#!/bin/bash
sleep 30
sudo yum install -y nginx
sudo service nginx start
sudo wget https://s3.amazonaws.com/izovskih-staticwebsite/index.html
sudo cp index.html /usr/share/nginx/html/index.html
sudo sed -i '/listen / s/\(80\)/8080/g' /etc/nginx/nginx.conf
sudo service nginx restart