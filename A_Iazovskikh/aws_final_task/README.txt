
Short Description
  This project create a public internet-facing load balancer and attach backend Amazon EC2 
instances that are not publicly reachable, instances that are in a private subnet.
  For this goal we create public subnets in the same Availability Zones as the private
subnets that are used by your private instances. Then associate these public subnets to
the internet-facing load balancer.

Frontend: an ALB which listen 80 HTTP and forward to backend. 
Backend:  EC2 instance (Amazon Linux) with nginx which listen 8080 HTTP. In the
         nginx root dir placed simple index.html with link to .jpeg which
         stored in s3 stiatic web site bucket. 

First, you should change some variables in "variables.tf" which placed in root dir aws_final_task:
  1. Define aws "region". I added only 3 regions but you can add all.
  2. You can also define Availability zones.
  3. AMI should be Linux distributions that are based on the source code of (RHEL)
  4. Environment can be different word, for example "PROD", "DEV", "TEST" etc...
  5. Define a SSH key.
  6. Specify path to user_data.sh

Usage:
  1. terraform init
  2. terraform plan
  3. terraform apply
