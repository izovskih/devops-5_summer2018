terraform {
  required_version = "> 0.9.0"
}
provider "aws" {
    region  = "${var.region}"
    profile = "default"
}
module "iam" {
    source                          = "modules/iam"
    name                            = "ModuleIAM"
    region                          = "${var.region}"
    environment                     = "${var.environment}"

    aws_iam_role-principals         = [
        "ec2.amazonaws.com",
    ]
    aws_iam_policy-actions           = [
        "cloudwatch:GetMetricStatistics",
        "logs:DescribeLogStreams",
        "logs:GetLogEvents",
        "elasticache:Describe*",
        "rds:Describe*",
        "rds:ListTagsForResource",
        "ec2:DescribeAccountAttributes",
        "ec2:DescribeAvailabilityZones",
        "ec2:DescribeSecurityGroups",
        "ec2:DescribeVpcs",
        "ec2:Owner",
    ]
}
module "vpc" {
    source                              = "modules/vpc"
    name                                = "ModuleVPC"
    environment                         = "${var.environment}"
    # VPC
    instance_tenancy                    = "default"
    enable_dns_support                  = "true"
    enable_dns_hostnames                = "true"
    assign_generated_ipv6_cidr_block    = "false"
    enable_classiclink                  = "false"
    vpc_cidr                            = "172.17.0.0/16"
    private_subnet_cidrs                = ["172.17.1.0/24", "172.17.2.0/24"]
    public_subnet_cidrs                 = ["172.17.0.0/24", "172.17.10.0/24"]
    availability_zones                  = ["${split(",", (lookup(var.availability_zones, var.region)))}"] #["us-east-1a", "us-east-1b"]
    enable_all_egress_ports             = "true"
    allowed_ports                       = ["8080", "80"]

    map_public_ip_on_launch             = "true"

    #Internet-GateWay
    enable_internet_gateway             = "true"
    #NAT
    enable_nat_gateway                  = "true"
    single_nat_gateway                  = "false"
    #VPN
    enable_vpn_gateway                  = "false"
    #DHCP
    enable_dhcp_options                 = "true"
    # EIP
    enable_eip                          = "false"
}
module "alb" {
    source                  = "modules/alb"
    name                    = "ModuleALB"
    region                  = "${var.region}"
    environment             = "${var.environment}"

    load_balancer_type          = "application"
    security_groups             = ["${module.vpc.security_group_id}", "${module.vpc.default_security_group_id}"]
    subnets                     = ["${module.vpc.vpc-publicsubnet-ids}"]
    vpc_id                      = "${module.vpc.vpc_id}"
    enable_deletion_protection  = false

    backend_protocol    = "HTTP"
    backend_port        = 8080

    alb_protocols       = "HTTP"

    target_ids          = []
}
module "asg" {
    source                              = "modules/asg"
    name                                = "ModuleASG"
    region                              = "${var.region}"
    environment                         = "${var.environment}"
    key_path                            = "${var.key_path}"

    security_groups = ["${module.vpc.security_group_id}"]
    
    root_block_device  = [
        {
            volume_size = "8"
            volume_type = "gp2"
        },
    ]
    placement_tenancy        = "${module.vpc.instance_tenancy}"

    vpc_zone_identifier       = ["${module.vpc.vpc-privatesubnet-ids}"]
    health_check_type         = "EC2"
    asg_min_size              = 2
    asg_max_size              = 4
    desired_capacity          = 2
    wait_for_capacity_timeout = 0

    # ALB
    load_balancer_type        = "ALB"
    #It's not working properly when use ALB... First of all, comment the line under this text. Run playbook. Uncomment that line:
    #load_balancers            = ["${module.alb.target_group_arn}"]
    target_group_arns           = ["${module.alb.target_group_arn}"]
    monitoring                = "true"

    enable_autoscaling_schedule = true
}
module "cpualarm-up" {
    source              = "modules/cloudwatch"

    alarm_name          = "monitor EC2 instance cpu utilization"
    alarm_description   = "This metric monitor EC2 instance cpu utilization"
    alarm_actions       = ["${module.asg.this_autoscaling_group_arn}"]
    
    comparison_operator = "GreaterThanOrEqualToThreshold"
    evaluation_periods  = "2"
    metric_name         = "CPUUtilization"
    namespace           = "AWS/EC2"
    period              = "120"
    statistic           = "Average"
    threshold           = "60"

    dimensions = [
        {
          AutoScalingGroupName = "${module.asg.autoscaling_policy_scale_up_arn}"
        }
    ]
}
module "cpualarm-down" {
    source              = "modules/cloudwatch"

    alarm_name          = "monitor EC2 instance cpu utilization - down"
    alarm_description   = "This metric monitor EC2 instance cpu utilization"
    alarm_actions       = ["${module.asg.this_autoscaling_group_arn}"]

    comparison_operator = "LessThanOrEqualToThreshold"
    evaluation_periods  = "2"
    metric_name         = "CPUUtilization"
    namespace           = "AWS/EC2"
    period              = "120"
    statistic           = "Average"
    threshold           = "10"

    dimensions = [
        {
          AutoScalingGroupName = "${module.asg.autoscaling_policy_scale_down_arn}"
        }
    ]
}